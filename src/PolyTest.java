import java.util.ArrayList;
import java.util.Iterator;

public class PolyTest {
	ArrayList<TermImp> terms;
	String polynomialString;
	
	public PolyTest(String s) {
	String str;
	int c=0;
	int e=0;
	String[] termArr;
	String[] coefExpArr;
	polynomialString = s;	
		
		if((s.charAt(0)=='x')||(s.charAt(0)=='-'&&s.charAt(1)=='x')) {
			s=s.replaceFirst("x", "1x");
		}
	
		str = s.replaceAll("-", "+-");
		str = str.replaceAll("x\\^", "x");
		str=str.replaceAll("x\\+", "x1+");
		termArr = str.split("\\+");
		
		for(int i=0;i<termArr.length;i++) {
			coefExpArr=termArr[i].split("x");	
			if(s.charAt(0)=='-') {
				for(int j=0;j<coefExpArr.length;j++) {					
					if(i==0 && j==0) {
						break;
					}
					else {
						if(coefExpArr.length==1) {
							c=Integer.parseInt(coefExpArr[j]);
							e=0;
						}
						else {
							c=Integer.parseInt(coefExpArr[j]);
							e=Integer.parseInt(coefExpArr[++j]);
						}
					}			
				TermImp termino = new TermImp(c,e);
				System.out.println("c="+c);
				System.out.println("e="+e);
				System.out.println();				
				}		
			}
			else {
				for(int j=0;j<coefExpArr.length;j++) {					
						if(coefExpArr.length==1) {
							c=Integer.parseInt(coefExpArr[j]);
							e=0;
						}
						else {
							c=Integer.parseInt(coefExpArr[j]);
							e=Integer.parseInt(coefExpArr[++j]);
						}					
				TermImp termino = new TermImp(c,e);
				System.out.println("c="+c);
				System.out.println("e="+e);
				System.out.println();				
				}
			}			
		}
	}
	
	public Iterator<Term> iterator() {
		
		return null;
	}

	public PolyTest add(PolyTest P2) {
		
		return null;
	}
	
	public PolyTest subtract(PolyTest P2) {
		
		return null;
	}
	
	public PolyTest multiply(PolyTest P2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public PolyTest multiply(double c) {
		// TODO Auto-generated method stub
		return null;
	}

	public PolyTest derivative() {
		// TODO Auto-generated method stub
		return null;
	}

	public PolyTest indefiniteIntegral() {
		// TODO Auto-generated method stub
		return null;
	}

	public double definiteIntegral(double a, double b) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public int degree() {
		// TODO Auto-generated method stub
		return 0;
	}

	public double evaluate(double x) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public boolean equals(PolyTest P) {
		// TODO Auto-generated method stub
		return false;
	}
		
}
