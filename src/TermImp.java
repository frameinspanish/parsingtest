
public class TermImp implements Term{
	int coefficient;
	int exponent;
	
	public TermImp(int a, int b) {
		coefficient = a;
		exponent = b;
	}

	public void setCoeff(int a) {
		coefficient = a;
	}
	
	public void setExp(int a) {
		exponent = a;
	}
	
	@Override
	public double getCoefficient() {
		return coefficient;
	}

	@Override
	public int getExponent() {
		return exponent;
	}

	@Override
	public double evaluate(double x) {
		return coefficient*Math.pow(x, exponent);
	}

}
